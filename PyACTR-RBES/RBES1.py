'''
Ben Langley Gary Tse
Python ACT-R Agent
csci379

September 6th, 2018
'''

# import ccm module library for Python ACT-R classes
import ccm
from ccm.lib.actr import *

class SandwichBuilder(ACTR):
    goal = Buffer()
    sandwich = []

    def init():
        goal.set("build_sandwich 1 prep")

    def prep_ingredients(goal="build_sandwich 1 prep"):
        #start building our sandwich!
        goal.set("build_sandwich 2 bottom_bread")

    def place_bottom_bread(goal="build_sandwich 2 ?ingred"):
        #Place the bottom piece of bread
        sandwich.append(ingred)
        goal.set("build_sandwich 3 mayo")

    def place_mayo(goal="build_sandwich 3 ?ingred"):
        # Place the ingriedient on the sandwich
        sandwich.append(ingred)
        goal.set("build_sandwich 4 turkey")

    def place_turkey(goal="build_sandwich 4 ?meat"):
        # Place the meat on the sandwich
        sandwich.append(meat)
        goal.set("build_sandwich 5 pepperjack")

    def place_cheese(goal="build_sandwich 5 ?cheese"):
        # Place the cheese on the sandwich
        sandwich.append(cheese)
        goal.set("build_sandwich 6 top_bread")

    def forgot_mayo(goal="build_sandwich 6 top_bread"):
        # Only add top bread if we have mayo
        if ('mayo' in sandwich):
            goal.set("build_sandwich 7 top_bread")
        else:
            goal.set("build_sandwich 6 mayo")

    def place_mayo_top(goal="build_sandwich 6 mayo"):
        # Oops we forgot the mayo
        sandwich.append('mayo')
        goal.set("build_sandwich 7 top_bread")

    def place_top_bread(goal="build_sandwich 7 top_bread"):
        sandwich.append("top bread")
        print("Sandwich Complete!")
        print(sandwich)
        self.stop()

class EmptyEnvironment(ccm.Model):
	pass

env_name = EmptyEnvironment()
agent_name = SandwichBuilder()
env_name.agent = agent_name
ccm.log_everything(env_name)
env_name.run()

