'''
Ben Langley
csci379
RBES Assignment
Sep 10th, 2018
'''

import ccm
log=ccm.log()

from ccm.lib import grid
from ccm.lib.actr import *

import random
import time

# Generate the map of the room
mymap="""
################
#             C#
#     D        #
#              #
##            ##
#   DD         #
#        D    D#
#            DD#
################
"""


#####
# Class to define the cells
# grid.cell.diry
#####
class MyCell(grid.Cell):
        dirty=False
        chargingsquare=False
        def color(self):
                if self.chargingsquare: return "green"
                elif self.dirty: return 'brown'
                elif self.wall: return 'black'
                else: return 'white'

        def load(self,char):
                if char=='#': self.wall=True
                elif char=='D': self.dirty=True
                elif char=='C': self.chargingsquare=True

#####
# Controls the movement of the robot
# as well as making the robot clean
#####
class MotorModule(ccm.Model):
        FORWARD_TIME = .1
        TURN_TIME = 0.025
        CLEAN_TIME = 0.025

        def __init__(self):
                ccm.Model.__init__(self)
                self.busy=False

        def turn_left(self, amount=1):
                if self.busy: return
                self.busy=True
                self.action="turning left"
                yield MotorModule.TURN_TIME
                amount *= -1
                self.parent.body.turn(amount)
                self.busy=False

        def turn_right(self, amount=1):
                if self.busy: return
                self.busy=True
                self.action="turning left"
                yield MotorModule.TURN_TIME
                self.parent.body.turn(amount)
                self.busy=False

        def turn_around(self):
                if self.busy: return
                self.busy=True
                self.action="turning around"
                yield MotorModule.TURN_TIME
                self.parent.body.turn_around()
                self.busy=False

        def go_forward(self, dist=1):
                if self.busy: return
                self.busy=True
                self.action="going forward"
                for i in range(dist):
                        yield MotorModule.FORWARD_TIME
                        self.parent.body.go_forward()
                self.action=None
                self.busy=False

        def go_left(self,dist=1):
                if self.busy: return
                self.busy="True"
                self.action='turning left'
                yield MotorModule.TURN_TIME
                self.parent.body.turn_left()
                self.action="going forward"
                for i in range(dist):
                        yield MotorModule.FORWARD_TIME
                        self.parent.body.go_forward()
                self.action=None
                self.busy=False

        def go_right(self):
                if self.busy: return
                self.busy=True
                self.action='turning right'
                yield 0.1
                self.parent.body.turn_right()
                self.action='going forward'
                yield MotorModule.FORWARD_TIME
                self.parent.body.go_forward()
                self.action=None
                self.busy=False

        def go_towards(self,x,y):
                if self.busy: return
                self.busy=True
                self.clean_if_dirty()
                self.action='going towards %s %s'%(x,y)
                yield MotorModule.FORWARD_TIME
                self.parent.body.go_towards(x,y)
                self.action=None
                self.busy=False

        def clean_if_dirty(self):
                "Clean cell if dirty"
                if (self.parent.body.cell.dirty):
                        self.action="cleaning cell"
                        self.clean()

        def clean(self):
                yield MotorModule.CLEAN_TIME
                self.parent.body.cell.dirty=False



#####
# Access to the robot senesors
# Provides methods to determine
# which directions are clear
#####
class ObstacleModule(ccm.ProductionSystem):
        production_time=0

        def init():
                self.ahead=body.ahead_cell.wall
                self.left=body.left90_cell.wall
                self.right=body.right90_cell.wall
                self.left45=body.left_cell.wall
                self.right45=body.right_cell.wall


        def check_ahead(self='ahead:False',body='ahead_cell.wall:True'):
                self.ahead=True

        def check_left(self='left:False',body='left90_cell.wall:True'):
                self.left=True

        def check_left45(self='left45:False',body='left_cell.wall:True'):
                self.left45=True

        def check_right(self='right:False',body='right90_cell.wall:True'):
                self.right=True

        def check_right45(self='right45:False',body='right_cell.wall:True'):
                self.right45=True

        def check_ahead2(self='ahead:True',body='ahead_cell.wall:False'):
                self.ahead=False

        def check_left2(self='left:True',body='left90_cell.wall:False'):
                self.left=False

        def check_left452(self='left45:True',body='left_cell.wall:False'):
                self.left45=False

        def check_right2(self='right:True',body='right90_cell.wall:False'):
                self.right=False

        def check_right452(self='right45:True',body='right_cell.wall:False'):
                self.right45=False

#####
# Provides access to the sensor
# which determines if the tile
# is clean or dirty
#####
class CleanSensorModule(ccm.ProductionSystem):
        production_time = 0
        dirty=False

        def found_dirty(self="dirty:False", body="cell.dirty:True"):
                self.dirty=True

        def found_clean(self="dirty:True", body="cell.dirty:False"):
                self.dirty=False


#####
# The RBES for the robot
# Goal based RBES which controls
# the flow of the robot using
# movement functions described
# above.
#####
class VacuumAgent(ACTR):
        goal = Buffer()
        body = grid.Body()
        motorInst = MotorModule()
        cleanSensor = CleanSensorModule()

        def init():
                goal.set("rsearch left 1 0 1")
                self.home = None

        #----ROOMBA----#

        # Clean cell if the clean sensor registers dirty
        def clean_cell(cleanSensor="dirty:True", utility=0.6):
                motorInst.clean()
                goal.set("rsearch left 1 0 1") # My own addition

        # Proceed forward!
        def forward_rsearch(goal="rsearch left ?dist ?num_turns ?curr_dist",
                                                motorInst="busy:False", body="ahead_cell.wall:False"):
                # Move Roomba forward by one tile
                motorInst.go_forward()
                print(body.ahead_cell.wall)
                # Update the distance moved
                curr_dist = str(int(curr_dist) - 1)
                goal.set("rsearch left ?dist ?num_turns ?curr_dist")

        # Search to the left!
        def left_rsearch(goal="rsearch left ?dist ?num_turns 0", motorInst="busy:False",
                                        utility=0.1):
                # Turn Roomba left 90 deg
                motorInst.turn_left(2)
                # Update the turns executed
                num_turns = str(int(num_turns) + 1)
                # If even number of turns it is time to increment the curr_dist
                curr_dist = str(int(num_turns) // 2 + 1)
                goal.set("rsearch left ?dist ?num_turns ?curr_dist")

        # Begin the wall follow!
        def follow(goal="rsearch left ?dist ?num_turns ?curr_dist", motorInst="busy:False", body="ahead_cell.wall:True"):
                # Turn Rooma left 90 deg
                motorInst.turn_left(2)
                # Follow wall for 6 units
                goal.set("follow 0 6")

        # Continue to wall follow!
        def follow_wall(goal="follow ?dist ?curr_dist", motorInst="busy:False", body="ahead_cell.wall:False"):
                # Move Roomba forward by one tile
                motorInst.go_forward()
                # Update the distance moved
                curr_dist = str(int(curr_dist) - 1)
                goal.set("follow ?dist ?curr_dist")

        # Hug wall during wall follow!
        def hit_wall_follow(goal="follow ?dist ?curr_dist", motorInst="busy:False", body="ahead_cell.wall:True"):
                # Check if we can turn left
                if(body.left90_cell.wall):
                    # Wall to left try turning right
                    if(body.right90_cell.wall):
                        # Wall to right turn around
                        motorInst.turn_around()
                    else:
                        # Can turn right
                        motorInst.turn_right(2)
                else:
                    # Turn left
                    motorInst.turn_left(2)

                goal.set("follow ?dist ?curr_dist")

        # End of wall follow do a random turn!
        def turn_follow(goal="follow ?dist 0", motorInst="busy:False", utility=0.1):
                # A random number to turn
                turn = random.randint(1, 3)

                # Determine which side the wall is on
                if(body.left90_cell.wall):
                    # Wall is on the left
                    motorInst.turn_right(turn)
                else:
                    # Wall is on the right
                    motorInst.turn_left(turn)

                goal.set("straight 0 6")

        # Move straight!
        def straight(goal="straight ?dist ?curr_dist", motorInst="busy:False", body="ahead_cell.wall:False"):
                # Move Roomba straight
                motorInst.go_forward()

                # Update the distance moved
                curr_dist = str(int(curr_dist) - 1)
                goal.set("straight ?dist ?curr_dist")

        # Bounce of wall while moving straight!
        def bounce(goal="straight ?dist ?curr_dist", motorInst="busy:False", body="ahead_cell.wall:True"):
                # A random number to turn
                turn = random.randint(1,3)

                # Turn left
                motorInst.turn_left(turn)

                goal.set("straight ?dist ?curr_dist")

        # Go back to doing a spiral!
        def start_spiral(goal="straught ?dist 0", motorInst="busy:False", utility=0.1):
                goal.set("rsearch left 1 0 1")




                ###Other stuff!

########################
#                      #
# Start the enviorment #
#                      #
########################
time.sleep(2) # Give Ben time to open up window to see what is happening
world=grid.World(MyCell,map=mymap)
agent=VacuumAgent()
agent.home=()
world.add(agent,5,5,dir=0,color="black")

ccm.log_everything(agent)
ccm.display(world)
world.run()
