Ben Langley
CSCI379
RBES Lab
September 10th

Sandwich
==============================
Run with: $ python3 RBES1.py

This makes a sandiwch yo

Roomba
===============================
Run with: $ python3 RBES_Roomba.py

Basic Flow of the Roomba
1) Begin in a left hand spiral until we hit a wall
2) Follow the wall for a set dist
    - If we hit a wall during this time then we turn
    - Continue to follow wall
3) Choose a random angle and turn
    - Proceed straight until we hit a wall or set dist
    - Bounce off wall at random angle
4) Repeat (go back to making a spiral)


My Creative addition:
When I make a mess they are typically not random spots, they come in clusters. So when the robot cleans a dirty spot it will begin doing the spiral again to hit the tiles close by. On this test enviorment this actually proved to be much quicker. There was only one tile left at the end and it was up to the randomness of the angle to modify it
