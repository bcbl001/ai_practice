# Some potentially useful modules
# Whether or not you use these (or others) depends on your implementation!
import random
import math
import os
import numpy as np
import matplotlib.pyplot as plt

class NeuralMMAgent(object):
    '''
    Class to for Neural Net Agents
    '''

    def __init__(self, num_in_nodes, num_hid_nodes, num_hid_layers, num_out_nodes, \
                learning_rate = 0.2, max_epoch=10000, max_sse=.01, momentum=0.2, \
                creation_function=None, activation_function=None, random_seed=1):
        '''
        Arguments:
            num_in_nodes -- total # of input layers for Neural Net
            num_hid_nodes -- total # of hidden nodes for each hidden layer
                in the Neural Net
            num_hid_layers -- total # of hidden layers for Neural Net
            num_out_nodes -- total # of output layers for Neural Net
            learning_rate -- learning rate to be used when propagating error
            creation_function -- function that will be used to create the
                neural network given the input
            activation_function -- list of two functions:
                1st function will be used by network to determine activation given a weighted summed input
                2nd function will be the derivative of the 1st function
            random_seed -- used to seed object random attribute.
                This ensures that we can reproduce results if wanted
        '''

        # Verify the user input data is valid
        assert num_in_nodes > 0, "Illegal number of input nodes"
        assert num_hid_nodes > 0, "Illegal number of hidden nodes"
        assert num_out_nodes > 0, "Illegal number of output nodes"
        assert num_hid_layers > 0, "Illegal number of hidden layers"
        assert num_hid_nodes % num_hid_layers == 0, "Illegal combination of hidden layers and nodes. Must have equal number of nodes per layer"

        self.num_in_nodes = num_in_nodes
        self.num_hid_nodes = num_hid_nodes
        self.num_hid_layers = num_hid_layers
        self.num_out_nodes = num_out_nodes
        self.learning_rate = learning_rate
        self.max_epoch = max_epoch
        self.max_sse = max_sse
        self.momentum = momentum
        self.creation_function = creation_function
        self.activation_function = activation_function
        self.random_seed = random_seed
        random.seed(self.random_seed)

        # Initialize the lists to be empty
        self.current_input = []
        self.current_output = []
        self.weights_old = []
        self.nodes = []
        self.weights = []
        self.thetas = []
        self.deltas = []
        self.outputs = []
        self.errors = []
        self.all_err = []

        self._initialize_neural_structure()


    def train_net(self, input_list, output_list, max_num_epoch=100000, max_sse=0.1):
        ''' Trains neural net using incremental learning
            (update once per input-output pair)
            Arguments:
                input_list -- 2D list of inputs
                output_list -- 2D list of outputs matching inputs
        '''

        # Train the network on each (input, output) pair provided
        # for the numberof epochs given
        for x in range(0, max_num_epoch):
            epoch_sse = []
            for i in range(0, len(input_list)):
                #print("Training Neural Net on ", input_list[i], " --> ", output_list[i])
                
                # Reset lists used for each training point
                self.outputs = []
                self.errors = []
                self.current_input = input_list[i]
                self.current_output = output_list[i]

                # Run the training data
                self._compute_initial_outputs() # Compute output functions
                self._calculate_deltas(output_list[i]) # Compute deltas
                self._adjust_weights_thetas() # Update weights

                # Check error
                sse = NeuralMMAgent.sse(self.errors)
                epoch_sse.append(sse)

            epoch_avg_sse = sum(epoch_sse)/len(epoch_sse) 
            epoch_sse = []
            self.all_err.append(epoch_avg_sse)
            if (epoch_avg_sse < max_sse):
                self._show_final_results(x, epoch_avg_sse)
                return

        self._show_final_results(x, epoch_avg_sse)


    def test_net(self, input_list, output_list):
        ''' Runs the inputs on the net and verifies the output
            from the network matched the output list
            Arguments:
                input_list -- a list of inputs (as a list for input to each in node)
                output_list -- a list of outputs (as a list for output to each out node)
        '''
        print("\nRunning Tests on Network")
        assert len(input_list) == len(output_list), "Input List and Output List mismatched size"

        # For each set of (input, output) compute the error of the outputs
        for i in range(0, len(input_list)):
            # Initialize the arrays
            self.outputs = []
            self.current_input = input_list[i]
            self.current_output = output_list[i]

            # Compute output functions
            self._compute_initial_outputs()
            output = self.outputs[len(self.outputs) - 1]

            # Compute error for each output
            error = (np.array(output_list[i]) - np.array(output)).tolist()
            sse = NeuralMMAgent.sse(error)

            # Print results
            print(input_list[i], "->", output_list[i], " Actual Output: ", output, "\n      Error: ", sse)


    def _calculate_deltas(self, out_desired):
        '''Used to calculate all weight deltas for our neural net
            Arguments:
                out_desired -- the desired output value of the training data
                    used to compute the error
        '''
        # List to store each layer's deltas 
        # the layers are in reverse order from typical indexing
        deltas_reverse = []

        # Delta for the output nodes
        delta_outputs = []
        error_outputs = []
        for i in range(0, self.num_out_nodes):
            # Compute delta = y(x) * (1 - y(x)) * error
            output_layer_index = self.getNumLayers() - 1
            output = self.outputs[output_layer_index - 1][i]
            error = NeuralMMAgent.error(out_desired[i], output)
            delta = output * (1 - output) * error
            delta_outputs.append(delta)
            self.errors.append(error)

        deltas_reverse.append(delta_outputs)

        # Compute Delta of the hidden nodes
        for layer in range(0, self.num_hid_layers):
            current_layer = self.num_hid_layers - layer
            deltas_layer = []
            for node in range(0, self.getNodesPerHidLayer()):
                # Compute delta = y(x) * (1 - y(x)) * SUM(delta(j) * weight(i,j))
                output = self.outputs[current_layer - 1][node]
                prev_layer_deltas = deltas_reverse[len(deltas_reverse) - 1]
                in_weights = self.weights[current_layer][node::self.getNodesPerHidLayer()]
                delta = output * (1 - output) * np.dot(prev_layer_deltas, in_weights)
                deltas_layer.append(delta)
            deltas_reverse.append(deltas_layer)

        # Reverse the list and store in instance variable
        self.deltas = list(reversed(deltas_reverse))

        ## DEBUG:: Print the deltas matrix
        #print("Deltas")
        #NeuralMMAgent.printMatrix(self.deltas)


    def _adjust_weights_thetas(self):
        ''' Used to apply deltas and update the weights
            of the connections in the network
        '''
        ## DEBUG:: show the weights matrix before update
        #print("Weights before update")
        #NeuralMMAgent.printMatrix(self.weights)
        #print("Deltas")
        #NeuralMMAgent.printMatrix(self.deltas)
        #print("momentum: ", self.momentum)

        # Loop through each connection in the network
        for layer in range(0, len(self.weights)):
            layer_counter = 0 # Variable to store the location in the weights layer list
            for node in range(0, len(self.nodes[layer + 1])):
                # Update the theta for the current node
                theta_delta = (-1) * self.learning_rate * self.deltas[layer][node]
                self.update_theta(layer + 1, node, theta_delta)

            for node in range(0, len(self.nodes[layer])):
                # Loop through all nodes in the next layer to update the connections
                # for the weights 
                for end_node in range(0, len(self.nodes[layer + 1])):
                    # Check if we are on an input node
                    if (layer == 0):
                        output = self.current_input[node]
                    else:
                        output = self.outputs[layer - 1][node]
                    delta = self.deltas[layer][end_node]          

                    # Update weight += alpha * output(node) * delta(end_node)    
                    weight_delta = self.learning_rate * output * delta
                    self.update_weight(layer, layer_counter, weight_delta)
                    layer_counter += 1

                    # DEBUG:: print the current node and connection
                    #print("Layer: ", layer, " ", node, "->", end_node, " ", output, "*", delta)

        # DEBUG:: show the new weights matrix after update
        #print("Weights after update")
        #NeuralMMAgent.printMatrix(self.weights)


    def _initialize_neural_structure(self):
        ''' Initialize the weights, thetas, and nodes array
            for the neural network of the given size
        '''
        ##############################
        #### Set the weights
        ##############################
        # first layer has num_in_nodes * node per hid layer
        self.weights.append(self.generateRandomWeights(self.num_in_nodes * self.getNodesPerHidLayer()))
        # if num_hid_layers > 1 --> node per hid layer * node per hid layer
        for layer in range(0, self.num_hid_layers - 1):
            self.weights.append(self.generateRandomWeights(self.getNodesPerHidLayer() * self.getNodesPerHidLayer()))
        # last layer has node per hid layer * num_out_node
        self.weights.append(self.generateRandomWeights(self.num_out_nodes * self.getNodesPerHidLayer()))
        # Set the olds weights to the current weights
        self.weights_old = self.weights

        print("Initial Weights")
        NeuralMMAgent.printMatrix(self.weights)
        print("")


        ##############################
        #### Set the thetas and nodes
        ##############################
        initial_theta = 0
        # lists for each layer
        thetas_layer = []
        nodes_layer = []

        # set the input layer
        for i in range(0, self.num_in_nodes):
            thetas_layer.append(initial_theta)
            nodes_layer.append(i)
        self.thetas.append(thetas_layer)
        self.nodes.append(nodes_layer)

        # set the hidden layers
        for layer in range(0, self.num_hid_layers):
            thetas_layer = []
            nodes_layer = []
            for i in range(0, self.getNodesPerHidLayer()):
                thetas_layer.append(initial_theta)
                nodes_layer.append(i)
            self.thetas.append(thetas_layer)
            self.nodes.append(nodes_layer)

        # set the output layer
        thetas_layer = []
        nodes_layer = []
        for i in range(0, self.num_out_nodes):
            thetas_layer.append(initial_theta)
            nodes_layer.append(i)
        self.thetas.append(thetas_layer)
        self.nodes.append(nodes_layer)

        '''
        DEBUG:: print initital thetas and nodes
        print("Initial Thetas")
        NeuralMMAgent.printMatrix(self.thetas)
        
        print("Nodes")
        NeuralMMAgent.printMatrix(self.nodes)
        '''

    #-----Begin ACCESSORS-----#

    def _show_final_results(self, num_epochs, sse):
        # Our error is below the tolerance
        print("Final Weights after ", num_epochs, " epochs. (SSE = ", sse, ")")
        NeuralMMAgent.printMatrix(self.weights)
        print("")
        print("Final outputs")
        NeuralMMAgent.printMatrix(self.outputs)
        print("")

        plt.plot(range(0, len(self.all_err)), self.all_err)
        plt.show()
        return

    def _compute_initial_outputs(self):
        ### Compute the value of all the outputs
        #print("Computing initial outputs")

        # Loop through all of the nodes to compute output
        for layer in range(1, len(self.nodes)):
            layer_output = []
            for node in range(0, len(self.nodes[layer])):
                # Commpute the output and append it to list
                output = self.computeOutput(layer, node)
                layer_output.append(output)
                #print("Layer: ", layer, " Node: ", node, " Output: ", output)
            self.outputs.append(layer_output)

        # DEBUG:: Print the matrix
        #print("Initial Outputs")
        #NeuralMMAgent.printMatrix(self.outputs)
        #print("\n")

    def update_output(layer, node, output):
        ### Update the output value of a specific node
        self.outputs[layer][node] = output

    def set_nodes(self, nodes):
        ### Set the names of nodes by layer
        self.nodes = nodes

    def set_weights(self, weights):
        ''' Set the entire weights matrix from
            the given input
        '''
        self.weights = weights
        self.weights_old = weights

    def update_weight(self, i, j, weight_delta):
        ''' Update the weight of a specific connection
            between node i and j in the weights matric.
            The weight argument is given as a delta from 
            the current value of weight
        '''
        self.weights[i][j] += self.weights_old[i][j]*self.momentum + weight_delta
        self.weights_old[i][j] = self.weights[i][j]

    def set_thetas(self, thetas):
        ''' Set the entire thetas matrix from
            the guven input
        '''
        self.thetas = thetas

    def update_theta(self, layer, node, theta_delta):
        ''' Update the theta bias of a specific
            node in the theta matrix. The theta_delta
            argument is given as a delta from the 
            current value of theta
        '''
        self.thetas[layer][node] += theta_delta
	
    def getNumLayers(self):
        # Get the total number of layers in the network
        return self.num_hid_layers + 2 # hid layers + input + output

    def getNumNodes(self):
        # Get the total number of nodes in the network
        return self.num_in_nodes + self.num_hid_nodes + self.num_out_nodes

    def getNodesPerHidLayer(self):
        # Get the number of nodes for each hidden layer
        return self.num_hid_nodes // self.num_hid_layers
   
    def computeOutput(self, layer, node):
        ''' Compute the output function y for a given node
            Arguments:
                layer -- the layer number/index of the node
                node -- the node number in the given layer
        '''
        # The in_weights only for the given node

        # Check if we are in the input layer or not
        if (layer == 1):
            # Compute the output for the first hidden layer
            # as in_weights * node input data
            slice_num = self.getNodesPerHidLayer()
            in_weights = self.weights[layer - 1][node::slice_num]
            summed_input = np.dot(in_weights, self.current_input)
        else:
            # Compute the output for non-first layer node
            # as in_weights * output functions
            #NeuralMMAgent.printMatrix(self.weights[layer - 1])
            slice_num = len(self.weights[layer - 1])//self.getNodesPerHidLayer()
            in_weights = self.weights[layer - 1][node::slice_num]
            summed_input = np.dot(in_weights, self.outputs[layer - 2])

        # Subtract the node theta value
        summed_input -= self.thetas[layer][node]

        # Return the output function of the summed input
        if (math.isnan(summed_input)):
            print("Layer: ", layer, " Node: ", node, " SI: ", summed_input)
            os._exit(0)
        return NeuralMMAgent.sigmoid_af(summed_input)

    def generateRandomWeights(self, n):
        # Generate an array of length n
        weights = []
        for x in range(0, n):
            weights.append(random.uniform(0, 2))
        return weights

    #-----End ACCESSORS-----#


    @staticmethod
    def sigmoid_af(summed_input):
        #Sigmoid function
        return 1 / (1 + math.exp(-summed_input))

    @staticmethod
    def sigmoid_af_deriv(summed_input):
        #the derivative of the sigmoid function
        y = sigmoid_af(summed_input)
        return y * (1 - y)

    @staticmethod
    def error(desired, actual):
        # The error of the output function
        error = desired - actual
        return error

    @staticmethod
    def printMatrix(M):
        ### Print matrix M row by row
        for i in range(0, len(M)):
            print(M[i])

    @staticmethod
    def sse(errors):
        # Compute the sum of squares of the errors
        return sum([x**2 for x in errors])

os.system('clear') # Clear the terminal window everytime to make it easier to read
##               num_in_nodes, num_hid_nodes, num_hid_layers, num_out_nodes,
#test_agent = NeuralMMAgent(3, 2, 1, 2,random_seed=3, max_epoch=100000, \
#                            learning_rate=0.5, momentum=0.0001, max_sse=.001)

# train XOR
'''
num_in_nodes = 2
num_hid_nodes = 4
num_hid_layers = 2
num_out_nodes = 1
test_in = [[1,0],[0,0],[1,1],[0,1]]
test_out = [[1],[0],[0],[1]]
'''
# train AND
#test_out = [[0],[0],[1],[0]]

# train OR
#test_out = [[1],[0],[1],[1]]

# train XOR AND OR
'''
num_in_nodes = 2
num_hid_nodes = 2
num_hid_layers = 1
num_out_nodes = 3
test_in = [[1,0],[0,0],[1,1],[0,1]]
test_out = [[1,0,1],[0,0,0],[0,1,1],[1,0,1]]
'''

# train (x1 AND x2 AND x3) (x1 OR x2 OR x3)

num_in_nodes = 3
num_hid_nodes = 4
num_hid_layers = 2
num_out_nodes = 2
test_in = [[1,0,0],[0,0,0],[1,1,0],[0,1,0],[1,0,1],[0,0,1],[1,1,1],[0,1,1]]
test_out = [[0,1],[0,0],[0,1],[0,1],[0,1],[0,1],[1,1],[0,1]]

test_agent = NeuralMMAgent(num_in_nodes, num_hid_nodes, num_hid_layers, num_out_nodes,\
                            random_seed=3, max_epoch=100000, \
                            learning_rate=0.5, momentum=0.00001, max_sse=.001)
test_agent.train_net(test_in, test_out, max_sse = test_agent.max_sse, max_num_epoch = test_agent.max_epoch)
test_agent.test_net(test_in, test_out)

## DEBUG:: Test on just the first set done in activity #1
'''
os.system('clear') # Clear the terminal window everytime to make it easier to read
test_agent = NeuralMMAgent(2, 2, 1, 1,random_seed=5, max_epoch=60000, \
                            learning_rate=0.18, max_sse=0.001, momentum=0.00019)
test_agent = NeuralMMAgent(2, 2, 1, 1,random_seed=5, max_epoch=1, \
                            learning_rate=0.2, max_sse=0.001, momentum=0)
test_in = [[1,0]]
test_out = [[1]]
test_agent.set_weights([[-.37,.26,.1,-.24],[-.01,-.05]])
print("Initial Weights")
test_agent.printMatrix(test_agent.weights)
print("Initial Nodes")
test_agent.printMatrix(test_agent.nodes)
print("Initial Thetas")
test_agent.printMatrix(test_agent.thetas)
test_agent.train_net(test_in, test_out, max_sse = test_agent.max_sse, max_num_epoch = test_agent.max_epoch)
test_agent.test_net(test_in, test_out)
'''




































# EOF