'''
Ben Langley

Description: A star graph search algorithm implementation

October 11th, 2018
'''

'''
Class to represent the Graph

Instance Variables
==================
nodes - list of nodes in the graph
edges - list of edges in the graph
'''
class Graph(object):

	def __init__(self, nodes, edges):
		self.nodes = nodes
		self.edges = edges


	'''
	Get details (node + weight) on the connections to the node

	node - the given node to find connetions to

	returns list of tuples in the form (connectedNode, weightToNode)
	'''
	def connectedNodesWeights(self, node):
		connectedNodes = []
		for edge in self.getConnectedEdges(node):
			connectedNode = edge.end_node if node == edge.start_node else edge.start_node
			connectedNodes.append((connectedNode, edge.weight))
		return connectedNodes


	'''
	Find all edges connected to the given node 

	node - the node to find edges with

	returns list of all edges where node is start or end node
	'''
	def getConnectedEdges(self, node):
		connectedEdges = []
		for edge in self.edges:
			if (edge.start_node == node) or (edge.end_node == node):
				connectedEdges.append(edge)
		return connectedEdges


	'''
	Find the edge between the two given nodes --> note the order
	of the nodes does not matter

	node_a - one node in the edge
	node_b - the other node in the edge

	returns the edge connecting node_a and node_b
		None if no edge found in graph
	'''
	def getEdge(self, node_a, node_b):
		for edge in self.edges:
			if ((edge.start_node == node_a) and (edge.end_node == node_b)) or ((edge.start_node == node_b) and (edge.end_node == node_a)):
				return edge
		return None

	'''
	Retrace the path of the fastest route to the given node
	from the start node of the AStar algorithm

	node - the node to retrace the path from (goal of A*)
	 
	returns list of path of nodes from start to (goal) node
	'''
	def retracePath(self, node):
		path = [node]
		while node.search_from_node != None:
			path.append(node.search_from_node)
			node = node.search_from_node
		path.reverse()
		return path


	'''
	Pop the minimum node from the frontier set

	returns the popped node object
	'''
	@staticmethod
	def __popFrontier(frontier):
		# Find minimum value on frontier
		minimum_node = list(frontier.keys())[0]
		for node, f in frontier.items():
			if (f < frontier[minimum_node]):
				minimum_node = node
		frontier.pop(minimum_node)
		return minimum_node
	

	'''
	Function to compute the Astar search for the given graph

	start - the starting node of the graph
	goal - the goal node of the graph to reach

	returns list of the path from start -> goal
	'''
	def AStar(self, start, goal):
		# Initialize the explored and froniter sets
		explored = {start} # set of explored states
		frontier = {} # dictionary of frontier and f values
		for node, weight in self.connectedNodesWeights(start):
			frontier[node] = node.h + weight
			node.search_from_node = start

		# Continue searching while we have nodes in the frontier
		while len(frontier) != 0:
			# Pop the node from frontier with lowest f cost
			current_node = Graph.__popFrontier(frontier)

			# Check if goal node
			if current_node == goal:
				break

			# Update the frontier to include the new nodes visible
			# which have not been explored and are not currently in
			# the frontier set
			for node, weight in self.connectedNodesWeights(current_node):
				# New node to add to frontier or update existing node
				if (node not in explored) and ((node not in frontier) or ((node in frontier) and (frontier[node] > node.h + weight))):
					frontier[node] = node.h + weight
					node.search_from_node = current_node

			# Update explored set
			explored.add(current_node)

		# Get the optimized path
		path = self.retracePath(current_node)
		return path



'''
Class to represent nodes in the Graph

Instance Variables
==================
h - the heuristic 
'''
class Node(object):

	def __init__(self, name, h, search_from_node=None):
		self.name = name
		self.h = h
		self.search_from_node = search_from_node



'''
Class to represent edges in the Graph

Instance Variables
==================
start_node - the tail of the edge
end_node - the head of the edge
weight - the cost to travel the edge
'''
class Edge(object):

	def __init__(self, start_node, end_node, weight):
		self.start_node = start_node
		self.end_node = end_node
		self.weight = weight



'''
Main function to test the AStar algorithm
'''
def main():
	# Create the nodes
	U = Node('U', 366)
	H = Node('H', 329)
	L = Node('L', 244)
	X = Node('X', 241)
	D = Node('D', 242)
	C = Node('C', 160)
	V = Node('V', 193)
	B = Node('B', 253)
	N = Node('N', 100)
	M = Node('M', 374)
	O = Node('O', 380)
	G = Node('G', 176)
	S = Node('S', 0)
	nodes = [U, H, L, X, D, C, V, B, N, M, O, G, S]

	# Create the edges
	edges = []
	edges.append(Edge(U,H,120))
	edges.append(Edge(H,L,10))
	edges.append(Edge(L,X,5))
	edges.append(Edge(X,D,50))
	edges.append(Edge(D,C,60))
	edges.append(Edge(C,V,35))
	edges.append(Edge(V,B,45))
	edges.append(Edge(U,B,110))
	edges.append(Edge(U,M,80))
	edges.append(Edge(M,O,100))
	edges.append(Edge(O,B,60))
	edges.append(Edge(B,G,20))
	edges.append(Edge(V,N,15))
	edges.append(Edge(N,S,200))
	edges.append(Edge(G,S,425))

	# Create the graph
	graph = Graph(nodes, edges)

	# Run the A* algorithm and get the path list of nodes
	path = graph.AStar(U, S)

	# Reconstruct the path in pretty form and compute final cost
	str_path = ""
	cost = 0
	for i in range(0, len(path) - 1):
		str_path += path[i].name + " -> "
		cost += graph.getEdge(path[i], path[i + 1]).weight
	str_path += path[i + 1].name
	print("Path: ", str_path)
	print("Cost: ", cost)


# Run the main method!!
main()






