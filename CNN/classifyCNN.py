'''
Description: Classification of the cifar10 trained model. Takes a directory parameter
and classifies all images in that directory based on the model saved to the file called
CIFAR10-datagen-trained-model.hd5

Usage
======
$ python3 classifyCNN.py [directory]
	directory - the directory containing the images to classify
		default is "images/"

Source: https://github.com/spmallick/learnopencv/blob/master/KerasCNN-CIFAR/keras-cnn-cifar10.ipynb
Blog: https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/

Ben Langley
October 4th, 2018
'''
from __future__ import print_function
from os import listdir
from PIL import Image
import numpy as np
import sys
import keras
from keras.preprocessing import image
from keras.models import Sequential, load_model
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.utils import to_categorical
labels = ["Airplane", "Automobile", "Bird", "Cat", "Deer", "Dog", "Frog", "Horse", "Ship", "Truck"]

'''
Helper function to decode the prediction matrix and print
out which labels are the classified class

prediction - the matrix of the model prediction
threshold - the cutoff value for classification [0, 1]
'''
def decodePrediction(prediction, threshold = 0.5):
	directory = getImageDirectory()
	images = getImageFiles(directory)
	for image in range(0, len(prediction)):
		i = 0
		print("Results for ", images[image])
		pred_list = prediction[image].tolist()
		pred = max(pred_list)
		index = pred_list.index(pred)
		print("   ", labels[index], ": ", pred)


'''
Retrieve a list of all the images in the supplied
directory to test the CNN with

directory - the directory path to search in

returns list of all files in the directory
'''
def getImageFiles(directory):
	return listdir(directory)


'''
Get the command line input for the directory
of photos to use

returns comman line input or default images
'''
def getImageDirectory():
	if (len(sys.argv) < 2):
		return "images"
	elif (len(sys.argv) > 2):
		print("Usage: $python3 classifyCNN.py [directory]")
		print("Using default directory")
		return "images"
	else:
		return sys.argv[1]


'''
Process the images and get array of the images

returns numpy array of image matrices
'''
def getImages():
	test_images = []
	directory = getImageDirectory()
	images = getImageFiles(directory)
	for file in images:
		path = directory + "/" + file
		img = image.load_img(path, target_size=(32, 32))
		x = image.img_to_array(img)
		x = np.expand_dims(x, axis=0)
		x /= 255 # normalize [0,1]
		test_images.append(x)
	return np.concatenate(test_images)


### Load the images to test
test_images = getImages()
print(test_images)



### Load the model
model = load_model("CIFAR10-datagen-trained-model.hd5")



### Test the model on the given image
prediction = model.predict(test_images)
decodePrediction(prediction)







