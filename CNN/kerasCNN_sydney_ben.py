'''
Description: Convolutional Neural Network for image classification using keras to 
create the NN. Trained on the images in the SydneyBen directory with subdirectories
Ben, Sydeny and Random which correspond to the classifications of those names. See 
links below for sources consulted in this project. 

This file trains the neural network and saves the model to sydney-ben-trained-model.hd5 
While the output model is hard coded you can supply the optional parameter of model_name which
will load the given hd5 model and resume training. The default is create a new empty model.
Batch size and num epochs are held constant at 256 and 200 respectively. These can be altered
within the code on lines 138 and 139

Usage
======
$ python3 kerasCNN.py [model_name]
    model_name - optional parameter for hd5 model to load and continue training
        if this parameter is not supplied a new model will be created



Source: https://github.com/spmallick/learnopencv/blob/master/KerasCNN-CIFAR/keras-cnn-cifar10.ipynb
Blog: https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/

Ben Langley
October 4th, 2018
'''
from __future__ import print_function
import matplotlib.pyplot as plt
from os import listdir
from PIL import Image
import numpy as np
import sys
import keras
from keras.preprocessing import image
from keras.models import Sequential, load_model
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.utils import to_categorical
labels = ["Sydney", "Ben", "Neither"]


'''
Retrieve a list of all the images in the supplied
directory to test the CNN with

directory - the directory path to search in

returns list of all files in the directory
'''
def getImageFiles(directory):
    return listdir(directory)


'''
Process the images and get array of the images

returns numpy array of image matrices
'''
def getImages(directory):
    test_images = []
    images = getImageFiles(directory)
    for file in images:
        if file == ".DS_Store":
            continue
        path = directory + "/" + file
        img = image.load_img(path, target_size=(32, 32))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x /= 255 # normalize [0,1]
        test_images.append(x)
    return np.concatenate(test_images)


'''
Concatentate arrays the correct way. Takes a list of lists
or a list of numpy arrays

arrays - lists of lists or lists of numpy arrays

returns numpy array of the arrays concatentated into one
'''
def concatenate(arrays):
    return_array = []
    for array in arrays:
        # Convert everything to a list
        if type(array) == np.ndarray:
            array = array.tolist()
        for element in array:
            return_array.append(element)
    return np.array(return_array)


### Define the model -- 3 sets of 2 convolutional layers and a max pool layer followed by
### a softmax layer
def createModel():
    model = Sequential()
    # The first two layers with 32 filters of window size 3x3
    model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=input_shape))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(labels), activation='softmax'))
    
    return model


'''
Get the model for the CNN either by creating a new one or loading the
model given as a command line argument

returns keras CNN model
'''
def getModel():
    if (len(sys.argv) < 2):
        print("Creating new model")
        sys.exit(0)
        return createModel()
    elif (len(sys.argv) > 2):
        print("Usage: $python3 kerasCNN_sydney_ben.py [model_name]")
        print("Using default model name")
        return createModel()
    else:
        print("Loading saved model ", sys.argv[1])
        return load_model(sys.argv[1])



### TODO: Fix the length here because it is inefficient
print("Getting Sydney Images...")
train_images_Sydney = getImages("SydneyBen/Sydney")
train_labels_Sydney = [0] * len(train_images_Sydney.tolist())
print(len(train_images_Sydney.tolist()), " images of Sydney retieved")
print("Getting Ben Images...")
train_images_Ben = getImages("SydneyBen/Ben")
train_labels_Ben = [1] * len(train_images_Ben.tolist())
print(len(train_images_Ben.tolist()), " images of Ben retieved")
print("Getting other Images...")
train_images_People = getImages("SydneyBen/People")
train_labels_People = [2] * len(train_images_People.tolist())
print(len(train_images_People.tolist()), " images of other people retieved")


train_images = concatenate([train_images_Sydney, train_images_Ben, train_images_People])
train_labels = concatenate([train_labels_Sydney, train_labels_Ben, train_labels_People])

### Preprocess the data to fit inputs/outputs - make the image data into
### 3D array of the pixel values and the output to be array of output nodes
### with a 1 indicating correct classification and 0 indicating incorrect
### classificiation for all of the other classes
# Find the shape of input images and create the variable input_shape
nRows,nCols,nDims = train_images.shape[1:]
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

# Change the labels from integer to categorical data
train_labels_one_hot = to_categorical(train_labels)



### Data Augmentation
from keras.preprocessing.image import ImageDataGenerator
datagen = ImageDataGenerator(
        zoom_range=0.2, # randomly zoom into images
        rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images
datagen.fit(train_data)



### Create and train the model
model = getModel()
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
batch_size = 256
epochs = 200

# Fit the model on the batches generated by datagen.flow()
history = model.fit_generator(datagen.flow(train_data, train_labels_one_hot, batch_size=batch_size),
                    steps_per_epoch=int(np.ceil(train_data.shape[0] / float(batch_size))),
                    epochs=epochs,
                    #validation_data=(test_data, test_labels_one_hot),
                    workers=4)


### Save the model and delete it
model.save("sydney-ben-trained-model.hd5")
del model



### Plot the accuracy curves
def plotLossAccuracy():
	plt.figure(figsize=[8,6])
	plt.plot(history.history['loss'],'r',linewidth=3.0)
	plt.plot(history.history['val_loss'],'b',linewidth=3.0)
	plt.legend(['Training loss', 'Validation Loss'],fontsize=18)
	plt.xlabel('Epochs ',fontsize=16)
	plt.ylabel('Loss',fontsize=16)

	plt.title('Loss Curves',fontsize=16)
	plt.show()

	plt.figure(figsize=[8,6])
	plt.plot(history.history['acc'],'r',linewidth=3.0)
	plt.plot(history.history['val_acc'],'b',linewidth=3.0)
	plt.legend(['Training Accuracy', 'Validation Accuracy'],fontsize=18)
	plt.xlabel('Epochs ',fontsize=16)
	plt.ylabel('Accuracy',fontsize=16)
	plt.title('Accuracy Curves',fontsize=16)
	plt.show()

#score = model.evaluate(test_data, test_labels_one_hot, verbose=0)
#print("%s: %.2f%%" % (model.metrics_names[1], score[1]*100))
#plotLossAccuracy() 