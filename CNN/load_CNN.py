'''
Description: File to test out loading a training hd5 model in keras. Loads the model
CIFAR10-datagen-trained-model.hd5 and runs a few tests

Usage
======
$ python3 load_CNN.py

Source: https://github.com/spmallick/learnopencv/blob/master/KerasCNN-CIFAR/keras-cnn-cifar10.ipynb
Blog: https://www.learnopencv.com/image-classification-using-convolutional-neural-networks-in-keras/

Ben Langley
October 4th, 2018
'''
from __future__ import print_function
import numpy as np
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras.utils import to_categorical


### Load the data from the CIFAR10 dataset
from keras.datasets import cifar10
(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()
labels = ["Airplane", "Automobile", "Bird", "Cat", "Deer", "Dog", "Frog", "Horse", "Ship", "Truck"]



### Preprocess the data to fit inputs/outputs - make the image data into
### 3D array of the pixel values and the output to be array of output nodes
### with a 1 indicating correct classification and 0 indicating incorrect
### classificiation for all of the other classes
# Find the shape of input images and create the variable input_shape
nRows,nCols,nDims = train_images.shape[1:]
train_data = train_images.reshape(train_images.shape[0], nRows, nCols, nDims)
test_data = test_images.reshape(test_images.shape[0], nRows, nCols, nDims)
input_shape = (nRows, nCols, nDims)

# Change to float datatype
train_data = train_data.astype('float32')
test_data = test_data.astype('float32')

# Scale the data to lie between 0 to 1
train_data /= 255
test_data /= 255

# Change the labels from integer to categorical data
train_labels_one_hot = to_categorical(train_labels)
test_labels_one_hot = to_categorical(test_labels)



### Load the model
model = load_model("CIFAR10-datagen-trained-model.hd5")



### Evaluate loaded model on test data
model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])
score = model.evaluate(test_data, test_labels_one_hot, verbose=0)
print("%s: %.2f%%" % (model.metrics_names[1], score[1]*100))


